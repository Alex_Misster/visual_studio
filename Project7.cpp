﻿// Project7.cpp : Этот файл содержит функцию "main". Здесь начинается и заканчивается выполнение программы.
//

#include <iostream>

class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "\n";
    }
};

class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Woof!\n";
    }
};

class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Myau!\n";
    }
};

class Sparrow : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Chirik Chirik!\n";
    }
};

int main()
{
    Animal* a1 = new Dog();
    Animal* a2 = new Cat();
    Animal* a3 = new Cat();
    a1->Voice();
    a2->Voice();
    a3->Voice();

    char arr[3];
    for (int i = 0; i < 3; i++)
    {
        arr[0] = a1;
        arr[1] = a2;
        arr[2] = a3;
        std::cout << arr[i];
    }
}
